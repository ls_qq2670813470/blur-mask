/*
 * @Author: ls
 * @Date: 2021-09-03 11:46:23
 * @LastEditTime: 2021-09-07 14:21:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \blur-mask\assets\scripts\Logic.ts
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class Logic extends cc.Component {
	@property({ type: cc.Node, tooltip: '根节点' })
	tapReceiver: cc.Node = null;

	@property({ type: cc.Node, tooltip: '模糊遮罩' })
	mask: cc.Node = null;

	@property({ type: cc.Node, tooltip: '弹框' })
	panel: cc.Node = null;

	showing: boolean = false;

	onLoad() {
		this.reset();
		this.tapReceiver.on('touchstart', () => (this.showing ? this.hide() : this.show()), this);
	}

	reset() {
		this.mask.opacity = 0;
		this.mask.active = false;

		this.panel.scale = 0.2;
		this.panel.opacity = 0;
		this.panel.active = false;
	}

	show() {
		this.showing = true;
		this.mask.active = true;
		this.panel.active = true;

		new Wheen(this.mask).to({ opacity: 255 }, 500, Wheen.Easing.Cubic.easeOut).start();
		new Wheen(this.panel).to({ opacity: 255, scale: 1 }, 500, Wheen.Easing.Back.easeOut).start();
	}

	hide() {
		this.showing = false;
		this.mask.active = true;
		this.panel.active = true;

		new Wheen(this.mask)
			.to({ opacity: 0 }, 500, Wheen.Easing.Cubic.easeOut)
			.on('finish', () => (this.mask.active = false))
			.start();
		new Wheen(this.panel)
			.to({ opacity: 0, scale: 0 }, 500, Wheen.Easing.Back.easeIn)
			.on('finish', () => (this.panel.active = false))
			.start();
	}
}
